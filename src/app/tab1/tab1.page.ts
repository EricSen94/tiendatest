import { Component } from '@angular/core';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss']
})
export class Tab1Page {
  array=[]
  constructor() {
    for(let i=0;i<10;i++){
      let aux=[]
      aux["name"]="name "+i
      aux["data"]="data "+i
      aux["hide"]=false;
      this.array.push(aux)
    }
  }
  archive(index){
    this.array[index].hide=true;
  }
  delete(index){
    this.array.splice(index,1)
  }
}
